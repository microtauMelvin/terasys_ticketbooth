const net = require('net'); // for tcp sockets

const socket_io = require('socket.io-client');
const socket = socket_io('https://terasys.circuitandcode.com', { path: '/app' });

const nS = require('./networkScan.js');  // my program to look for open 5025 pors

const piId = require('fs').readFileSync(require('path').join(__dirname, 'serial'), {encoding: 'utf-8'}); // just gives the pi an id

var openPorts = {}; // normally this is enumerated by the event below
// you can manually set it with the proper info instead of using the enumeration event
// the spaghetti just uses the first one available 

socket.on("enumeration", function(){ // find all scpi devices
		for (var k in openPorts) {
			if (openPorts.hasOwnProperty(k)){
				openPorts[k].socket.end();
			}
		}
	nS.scanLAN(function(result){ 
		var transmission = [];
		openPorts=result; 
		for (var k in openPorts) {
			if (openPorts.hasOwnProperty(k)){
				transmission.push(openPorts[k].stat);
			}
		}
		socket.emit("enumeration", transmission);
	});
});

socket.emit("pi-in", {id: piId}); // register yourself with server

socket.on("tx", function(data){ // commanded to write data to scpi
	console.log(data)
	console.log(data.device);
	openPorts[data.device].socket.write(data.tx)
});

// the "data" packets here are not well documents are standardized
// console log makes it easy
socket.on("OpenInst", function(data){ // server has commanded it to use 
	console.log(data);
	openPorts[data[0]].socket.connect(5025, data[0], function(){ // open tcp/ip socket
		console.log("new socket");
		console.log(data[0]);
		socket.emit("OpenInst", data); // basically saying "it worked" to server
		this.on('close', function(){
			console.log("Closing Socket");
			socket.emit("CloseInst", data);
		});
		this.setTimeout(0);
		this.on('end', function(){
			this.setTimeout(0);
			console.log("closed socket");
		});
		this.on('data', function (rx) { // commanded to write data from scpi-->server
			console.log("rx");
			console.log(rx);
			rx = Uint8Array.from(rx.slice(8, -1)); // prune off weird BINARY header and /n
			for (var k = 0; (k+8) < rx.length; k+=8){ // reverse byte order
				var t = rx[0+k];
				rx[0+k] = rx[7+k];
				rx[7+k] = t; 
				t = rx[1+k];
				rx[1+k] = rx[6+k]
				rx[6+k] = t;
				t = rx[2+k];
				rx[2+k] = rx[5+k]
				rx[5+k] = t;
				t = rx[3+k];
				rx[3+k] = rx[4+k]
				rx[4+k] = t;
			}	
			rx = Buffer.from(rx);
			socket.emit('rx', rx); // emit data
		});
		this.on('timeout', function (){
			console.log("socket timeout");
		});
		this.on('error', function(err){
			throw err;
		});
	});
});


