const net = require('net');
const os = require('os');
var openPorts = [];
var responsivePorts = {};
class Instrument {
	constructor(ip, serial){
		this.ip = ip;
		this.serial = serial;
		this.socket = new net.Socket();
		this.on = false;
	}
	get stat(){
		return [this.ip, this.serial, this.on];
	};
}

exports.scanLAN = scanNetwork;
exports.openClients = responsivePorts;
exports.Instrument = Instrument;
function scanNetwork(callbackFunc){
	console.log("Looking for interfaces...");
	var interfaces = os.networkInterfaces();
	var addresses = [];
	for (var k in interfaces){
		for (var k2 in interfaces[k]) {
			var address=interfaces[k][k2];
			if (address.family == 'IPv4' && !address.internal){
				AP = address.address.split(".");
				AP = AP[0] + "." + AP[1] + "." + AP[2] + ".";
				for (var k3 in addresses){
					if (addresses[k3] === AP){
						AP = "";
						break;
					}
				}
				if (AP != "") addresses.push(AP);
			}
		}
	}
	function allDone(){
		var size = openPorts.length;
		for (var k4 in openPorts){
			net.connect(5025, openPorts[k4].ip, function(){
				this.setEncoding('utf-8');
				this.on("data", function(data){
					if (data.search("Agilent") != -1) // needs a dictionary
					responsivePorts[openPorts[k4].ip] = (new Instrument(openPorts[k4].ip, data));
					this.setTimeout(0);
					this.end();
					if (--size == 0){
						callbackFunc.call(this,responsivePorts);
					}
					this.destroy();
				});
				this.write("*IDN?\n\0");// this should come from a SCPI wrapper- probably extends net?
				this.setTimeout(15000, function(){
					console.log("One time out")
					if (--size == 0){
						callbackFunc.call(this,responsivePorts);
					}
					this.end("\n\0");
					this.destroy();
				});
			});
		}
	}
	
	var counter = 0;
	function testPort(prefix, postfix){
		if (postfix > 255){
			if (++counter >= addresses.length){
				allDone();
				return;
			}
			testPort(addresses[counter], 0);	
			return;
		}
		address = prefix + postfix;
		var state = false; // we need a better state here
		var temporary = net.connect(5025, address, function(){
			if (!state) state = true;
			else this.end();
			openPorts.push({ip:address});
			testPort(prefix, postfix+1);
			this.destroy();
		});
		temporary.on("error", function(err){
			if (!state) state = true;
			else this.destroy();
			testPort(prefix,postfix+1);
			this.destroy();
		});
		temporary.setTimeout(3, function(){
			if (!state) state = true;
			else this.destroy();
			testPort(prefix, postfix+1);
			this.destroy();
		});
	}
	testPort(addresses[counter], 0);
}
